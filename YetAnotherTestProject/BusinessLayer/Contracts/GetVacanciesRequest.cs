﻿namespace BusinessLayer.Contracts {
    public class GetVacanciesRequest {
        public bool GetFromDb { get; set; }
        public string SiteUrl { get; set; }
    }
}