using System.Collections.Generic;
using Model.Entities;

namespace BusinessLayer.Contracts {
    public class GetVacanciesResponse {
        public IEnumerable<Vacancy> Vacancies { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsError { get; set; }
    }
}