﻿using System;
using System.Linq;
using BusinessLayer.Abstract;
using BusinessLayer.Impl;
using DataAccess.Abstract;
using HtmlAgilityPack;
using Model.Entities;
using Ninject;
using Ninject.Modules;

namespace BusinessLayer {
    public class BusinessLayerNinjectModule : NinjectModule {
        public override void Load() {
            Bind<HtmlWeb>().ToSelf();
            Bind<Func<string, ParserTemplate>>().ToMethod(context => str => {
                var repository = context.Kernel.Get<IRepository>();
                return repository.Get<ParserTemplate>(x => str.Contains(x.SiteUrl))?.SingleOrDefault();
            });

            Bind<IVacancyParser>().To<HHVacancyParser>().Named("hh.ru");
            Bind<Func<string, IVacancyParser>>().ToMethod(context => str => {
                var parser = context.Kernel.TryGet<IVacancyParser>(str);
                return parser;
            });

            Bind<IVacancyManager>().To<VacancyManager>();
        }
    }
}
