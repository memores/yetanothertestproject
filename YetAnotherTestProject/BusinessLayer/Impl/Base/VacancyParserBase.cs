using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer.Abstract;
using DataAccess.Abstract;
using HtmlAgilityPack;
using Model.Entities;

namespace BusinessLayer.Impl.Base {
    internal class VacancyParserBase : IVacancyParser {
        readonly HtmlWeb _web;
        readonly IRepository _repository;
        ParserTemplate _template;
        public virtual Func<string, ParserTemplate> GetTemplate { get; }


        public VacancyParserBase(HtmlWeb web, Func<string, ParserTemplate> getTemplate, IRepository repository) {
            GetTemplate = getTemplate;
            _web = web;
            _repository = repository;
        }


        public virtual IEnumerable<Vacancy> GetVacanciesFrom(string siteUrl) {
            var document = _web.Load(siteUrl);

            _template = GetTemplate(siteUrl);

            var vacancyContainers = document.DocumentNode.SelectNodes(_template?.VacancyContainersPath);
            if (vacancyContainers == null)
                return new List<Vacancy>(0);

            var vacancies = new List<Vacancy>();
            Parallel.ForEach(vacancyContainers, vacancyContainer => {
                var vacancy = GetOrCreateVacancy(vacancyContainer, siteUrl);
                if (vacancy == null)
                    return;

                vacancies.Add(vacancy);
            });

            return vacancies;
        }

        protected virtual Vacancy GetOrCreateVacancy(HtmlNode vacancyContainer, string siteUrl) {
            var url = vacancyContainer.SelectSingleNode(_template.UrlPath).Attributes["href"].Value;

            var vacancy = _repository.Get<Vacancy>(x => x.Url == url)?.SingleOrDefault();
            if (vacancy != null)
                return vacancy;

            vacancy = new Vacancy() {
                Id = Guid.NewGuid(),
                Title = vacancyContainer.SelectSingleNode(_template.TitlePath)?.InnerText,
                Url = url,
                Description = vacancyContainer.SelectSingleNode(_template.DescriptionPath)?.InnerText,
                Salary = vacancyContainer.SelectSingleNode(_template.SalaryPath)?.InnerText,
                EmploymentType = GetEmploymentType(url)
            };

            var company = GetOrCreateCompany(vacancy.Url);
            var contactPeson = GetOrCreateContactPerson(vacancy.Url);

            vacancy.Company = company;
            vacancy.ContactPerson = contactPeson;

            vacancy.CompanyId = company?.Id;
            vacancy.ContactPersonId = contactPeson?.Id;

            if (!_repository.Insert(vacancy))
                return null;

            return vacancy;
        }


        protected virtual string GetEmploymentType(string vacancyUrl) {
            var vacancyDocument = _web.Load(vacancyUrl);

            return vacancyDocument.DocumentNode.SelectSingleNode(_template?.EmploymentTypePath)?.InnerText;
        }

        protected virtual ContactPerson GetOrCreateContactPerson(string vacancyUrl) {
            var vacancyDocument = _web.Load(vacancyUrl);

            var phone = vacancyDocument.DocumentNode.SelectSingleNode(_template.ContactPersonPhonePath)?.InnerText;
            var name = vacancyDocument.DocumentNode.SelectSingleNode(_template.ContactPersonNamePath)?.InnerText;

            var contactPerson =
                _repository.Get<ContactPerson>(x => x.Phone == phone && x.Name == name)?.FirstOrDefault();
            if (contactPerson != null)
                return contactPerson;


            contactPerson = new ContactPerson() {
                Id = Guid.NewGuid(),
                Name = name,
                Phone = phone,
            };

            if (!_repository.Insert(contactPerson))
                return null;

            return contactPerson;
        }

        protected virtual Company GetOrCreateCompany(string vacancyUrl) {
            var vacancyDocument = _web.Load(vacancyUrl);

            var name = vacancyDocument.DocumentNode.SelectSingleNode(_template.CompanyNamePath)?.InnerText;

            var company = _repository.Get<Company>(x => x.Name == name)?.FirstOrDefault();
            if (company != null)
                return company;

            company = new Company() {
                Id = Guid.NewGuid(),
                Name = name
            };

            if (!_repository.Insert(company))
                return null;

            return company;
        }
    }
}