﻿using System;
using BusinessLayer.Impl.Base;
using DataAccess.Abstract;
using HtmlAgilityPack;
using Model.Entities;

namespace BusinessLayer.Impl {
    internal class HHVacancyParser : VacancyParserBase {
        public HHVacancyParser(HtmlWeb web, Func<string, ParserTemplate> getTemplate, IRepository repository) : base(web, getTemplate, repository) {}

        protected override ContactPerson GetOrCreateContactPerson(string vacancyUrl) {
            /*do nothing*/
            return null;
        }
    }
}