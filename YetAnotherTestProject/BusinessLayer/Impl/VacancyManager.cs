﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Abstract;
using BusinessLayer.Contracts;
using DataAccess.Abstract;
using Model.Entities;

namespace BusinessLayer.Impl
{
    internal class VacancyManager : IVacancyManager
    {
        private readonly IRepository _repository;
        public virtual Func<string, IVacancyParser> GetVacancyParser { get; }

        public VacancyManager(IRepository repository, Func<string, IVacancyParser> getVacancyParser) {
            _repository = repository;
            GetVacancyParser = getVacancyParser;
        }

        public GetVacanciesResponse GetVacancies(GetVacanciesRequest request) {
            if (request.GetFromDb)
                return new GetVacanciesResponse() {
                    ErrorMessage = "Загружено из основной БД",
                    IsError = false,
                    Vacancies = _repository.GetAll<Vacancy>()
                };
            

            var vacancyParser = GetVacancyParser(GetDomain(request.SiteUrl));
            if (vacancyParser == null)
                return new GetVacanciesResponse() {
                    ErrorMessage = $"Не удалось получить парсер для {request.SiteUrl}",
                    IsError = true,
                    Vacancies = _repository.GetAll<Vacancy>()
                };


            IEnumerable<Vacancy> vacancies;
            try {
                vacancies = vacancyParser.GetVacanciesFrom(request.SiteUrl);
            }
            catch (Exception e) {
                return new GetVacanciesResponse() {
                    ErrorMessage = $"Ошибка в парсере {vacancyParser.GetType()} {e.Message}",
                    IsError = true,
                    Vacancies = new List<Vacancy>(0)
                };
            }

            return new GetVacanciesResponse() {
                ErrorMessage = $"Загружено с сайта {request.SiteUrl}",
                IsError = false,
                Vacancies = vacancies
            };
        }


        private string GetDomain(string siteUrl) {
            var siteUri = new Uri(siteUrl);
            var host = siteUri.Host;
            if (string.IsNullOrWhiteSpace(host))
                return string.Empty;

            var splitHostName = host.Split('.');
            return (splitHostName.Length >= 2)
                ? $"{splitHostName[splitHostName.Length - 2]}.{splitHostName[splitHostName.Length - 1]}"
                : string.Empty;
        }
    }
}
