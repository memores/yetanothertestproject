﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BusinessLayer.Impl;
using DataAccess.Abstract;
using FakeItEasy;
using HtmlAgilityPack;
using Machine.Specifications;
using Model.Entities;

namespace BusinessLayer.Specs
{
    internal class HhVacancyParser_Specs {
        Establish context = () => {
            var parserTemplate = new ParserTemplate() {
                Id = Guid.NewGuid(),
                SiteUrl = "hh.ru",
                VacancyContainersPath =
                    "//div[contains(@class,'search-result-item search-result-item')]",
                CompanyNamePath = "//a[contains(@itemprop,'hiringOrganization')]",
                EmploymentTypePath = "//span[contains(@itemprop, 'employmentType')]",
                TitlePath =
                    ".//a[contains(@class,'search-result-item__name search-result-item__name')]",
                UrlPath =
                    ".//a[contains(@class,'search-result-item__name search-result-item__name')]",
                DescriptionPath = ".//div[contains(@class,'search-result-item__snippet')]",
                SalaryPath = ".//div[contains(@class,'b-vacancy-list-salary')]"
            };


            var fakeRepository = A.Fake<IRepository>();
            A.CallTo(() => fakeRepository.Insert(A<Vacancy>.Ignored)).Returns(true);
            A.CallTo(() => fakeRepository.Insert(A<Company>.Ignored)).Returns(true);
            A.CallTo(() => fakeRepository.Insert(A<ContactPerson>.Ignored)).Returns(true);
            A.CallTo(() => fakeRepository.Get<Vacancy>(A<Expression<Func<Vacancy, bool>>>.Ignored)).Returns(null);
            A.CallTo(() => fakeRepository.Get<Company>(A<Expression<Func<Company, bool>>>.Ignored)).Returns(null);
            A.CallTo(() => fakeRepository.Get<ContactPerson>(A<Expression<Func<ContactPerson, bool>>>.Ignored)).Returns(null);


            _vacancyParser = new HHVacancyParser(new HtmlWeb(), str => parserTemplate, fakeRepository);
        };

        Because of = () => _vacancies = _vacancyParser.GetVacanciesFrom("https://kaluga.hh.ru/search/vacancy?text=&area=1859");

        It should_return_vacancies = () => _vacancies.ShouldNotBeNull();

        It should_return_20_vacancies = () => _vacancies.Count().ShouldEqual(20);

        It should_contains_company_info = () => {
            _vacancies.FirstOrDefault().Company.ShouldNotBeNull();
            _vacancies.FirstOrDefault().Company.Name.ShouldNotBeEmpty();
        };

        It should_contains_vacancy_info = () => {
            _vacancies.FirstOrDefault().ShouldNotBeNull();
            _vacancies.FirstOrDefault().Title.ShouldNotBeEmpty();
            _vacancies.FirstOrDefault().Description.ShouldNotBeEmpty();
        };

        static HHVacancyParser _vacancyParser;
        static IEnumerable<Vacancy> _vacancies;
    }
}
