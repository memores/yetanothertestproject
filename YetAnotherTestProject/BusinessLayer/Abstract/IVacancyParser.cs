﻿using System.Collections.Generic;
using Model.Entities;

namespace BusinessLayer.Abstract
{
    public interface IVacancyParser
    {
        IEnumerable<Vacancy> GetVacanciesFrom(string siteUrl);
    }
}
