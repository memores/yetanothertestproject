﻿using BusinessLayer.Contracts;

namespace BusinessLayer.Abstract
{
    public interface IVacancyManager
    {
        GetVacanciesResponse GetVacancies(GetVacanciesRequest request);
    }
}