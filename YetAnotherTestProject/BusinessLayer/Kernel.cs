using Ninject;
using Ninject.Modules;

namespace BusinessLayer
{
    public class Kernel : StandardKernel
    {
        public Kernel(params INinjectModule[] modules) : base(modules)
        {
        }
    }
}