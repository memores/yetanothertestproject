﻿using Model.Base;

namespace Model.Entities
{
    public class ParserTemplate : Entity
    {
        public string SiteUrl { get; set; }

        public string VacancyContainersPath { get; set; }

        public string TitlePath { get; set; }

        public string UrlPath { get; set; }

        public string SalaryPath { get; set; }

        public string DescriptionPath { get; set; }

        public string EmploymentTypePath { get; set; }

        public string CompanyNamePath { get; set; }

        public string ContactPersonNamePath { get; set; }

        public string ContactPersonPhonePath { get; set; }
    }
}
