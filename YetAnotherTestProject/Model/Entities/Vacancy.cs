﻿using System;
using Model.Base;

namespace Model.Entities
{
    /// <summary>
    /// Информация о вакансии
    /// </summary>
    public class Vacancy : Entity
    {
        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Оклад
        /// </summary>
        public string Salary { get; set; }

        /// <summary>
        /// Организация разместившая вакансию
        /// </summary>
        public Guid? CompanyId { get; set; }

        /// <summary>
        /// Контактное лицо
        /// </summary>
        public Guid? ContactPersonId { get; set; }

        /// <summary>
        /// Тип занятости
        /// </summary>
        public string EmploymentType { get; set; }

        /// <summary>
        /// Описание вакансии
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// Ссылка на подробное описание
        /// </summary>
        public string Url { get; set; }


        public virtual Company Company { get; set; }
        public virtual ContactPerson ContactPerson { get; set; }
    }
}