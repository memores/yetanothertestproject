﻿using Model.Base;

namespace Model.Entities
{
    /// <summary>
    /// Организация
    /// </summary>
    public class Company : Entity
    {
        /// <summary>
        /// Название организации
        /// </summary>
        public string Name { get; set; }
    }
}