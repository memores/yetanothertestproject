﻿using Model.Base;

namespace Model.Entities
{
    /// <summary>
    /// Контактное лицо
    /// </summary>
    public class ContactPerson : Entity
    {
        /// <summary>
        /// Имя контактного лица
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }
    }
}
