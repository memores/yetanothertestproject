﻿using System;

namespace Model.Base
{
    public class Entity
    {
        public Guid Id { get; set; }
    }
}