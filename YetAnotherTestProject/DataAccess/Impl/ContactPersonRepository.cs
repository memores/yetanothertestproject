﻿using System;
using System.Data.Entity;
using DataAccess.Impl.Base;

namespace DataAccess.Impl
{
    internal class ContactPersonRepository : RepositoryBase
    {
        public ContactPersonRepository(Func<DbContext> context) : base(context)
        {
        }
    }
}
