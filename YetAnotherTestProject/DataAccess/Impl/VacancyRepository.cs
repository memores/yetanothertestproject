﻿using System;
using System.Data.Entity;
using DataAccess.Impl.Base;

namespace DataAccess.Impl
{
    internal class VacancyRepository : RepositoryBase {
        public VacancyRepository(Func<DbContext> context) : base(context) {}
    }
}
