﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using DataAccess.Abstract;

namespace DataAccess.Impl.Base
{
    internal class RepositoryBase : IRepository
    {
        public Func<DbContext> GetContext { get; set; }

        public RepositoryBase(Func<DbContext> getContext)
        {
            GetContext = getContext;
        }

        public virtual IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class
        {
            return GetContext().Set<TEntity>().ToList();
        }

        public virtual IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class
        {
            return GetContext().Set<TEntity>().Where(where).ToList();
        }

        public virtual bool Insert<TEntity>(TEntity entity) where TEntity : class
        {
            GetContext().Set<TEntity>().Add(entity);
            return GetContext().SaveChanges() > 0;
        }

        public virtual bool InsertMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            GetContext().Set<TEntity>().AddRange(entities);
            return GetContext().SaveChanges() > 0;
        }

        public virtual bool Update<TEntity>(TEntity entity) where TEntity : class
        {
            GetContext().Set<TEntity>().Attach(entity);
            GetContext().Entry(entity).State = EntityState.Modified;
            return GetContext().SaveChanges() > 0;
        }

        public virtual bool Delete<TEntity>(TEntity entity) where TEntity : class
        {
            GetContext().Set<TEntity>().Remove(entity);
            return GetContext().SaveChanges() > 0;
        }
    }
}