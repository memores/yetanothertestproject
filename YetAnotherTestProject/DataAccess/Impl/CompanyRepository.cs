﻿using System;
using System.Data.Entity;
using DataAccess.Impl.Base;

namespace DataAccess.Impl
{
    internal class CompanyRepository : RepositoryBase
    {
        public CompanyRepository(Func<DbContext> context) : base(context)
        {
        }
    }
}
