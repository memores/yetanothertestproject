﻿using System.Data.Entity;
using Model.Entities;

namespace DataAccess
{
    internal class AppContext : DbContext
    {
        public DbSet<Vacancy> Vacancies { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<ContactPerson> ContactPersons { get; set; }
        public DbSet<ParserTemplate> ParserTemplates { get; set; }
    }
}
