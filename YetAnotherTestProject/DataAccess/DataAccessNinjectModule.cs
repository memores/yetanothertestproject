﻿using System;
using System.Data.Entity;
using DataAccess.Abstract;
using DataAccess.Impl.Base;
using Ninject;
using Ninject.Modules;

namespace DataAccess
{
    public class DataAccessNinjectModule: NinjectModule
    {
        public override void Load()
        {
            Bind<DbContext>().To<AppContext>().InThreadScope();
            Bind<Func<DbContext>>().ToMethod(x => () => x.Kernel.Get<DbContext>());

            Bind<IRepository>().To<RepositoryBase>();
        }
    }
}
