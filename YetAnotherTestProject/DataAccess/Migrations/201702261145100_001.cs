namespace DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class _001 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContactPersons",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Phone = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ParserTemplates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SiteUrl = c.String(),
                        VacancyContainersPath = c.String(),
                        TitlePath = c.String(),
                        UrlPath = c.String(),
                        SalaryPath = c.String(),
                        DescriptionPath = c.String(),
                        EmploymentTypePath = c.String(),
                        CompanyNamePath = c.String(),
                        ContactPersonNamePath = c.String(),
                        ContactPersonPhonePath = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vacancies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Salary = c.String(),
                        CompanyId = c.Guid(),
                        ContactPersonId = c.Guid(),
                        EmploymentType = c.String(),
                        Description = c.String(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.ContactPersons", t => t.ContactPersonId)
                .Index(t => t.CompanyId)
                .Index(t => t.ContactPersonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vacancies", "ContactPersonId", "dbo.ContactPersons");
            DropForeignKey("dbo.Vacancies", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Vacancies", new[] { "ContactPersonId" });
            DropIndex("dbo.Vacancies", new[] { "CompanyId" });
            DropTable("dbo.Vacancies");
            DropTable("dbo.ParserTemplates");
            DropTable("dbo.ContactPersons");
            DropTable("dbo.Companies");
        }
    }
}
