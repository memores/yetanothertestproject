﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DataAccess.Abstract
{
    public interface IRepository
    {
        IEnumerable<TEntity> GetAll<TEntity>() where TEntity : class;


        IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> where) where TEntity : class;
        

        bool Insert<TEntity>(TEntity entity) where TEntity : class;


        bool InsertMany<TEntity>(IEnumerable<TEntity> entity) where TEntity : class;


        bool Update<TEntity>(TEntity entity) where TEntity : class;


        bool Delete<TEntity>(TEntity entity) where TEntity : class;
    }
}