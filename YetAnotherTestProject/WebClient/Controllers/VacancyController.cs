﻿using System;
using System.Web.Mvc;
using BusinessLayer.Abstract;
using BusinessLayer.Contracts;
using DataAccess.Abstract;
using Model.Entities;

namespace WebClient.Controllers {
    public class VacancyController : Controller {
        private readonly IVacancyManager _vacancyManager;

        public VacancyController(IVacancyManager vacancyManager) {
            _vacancyManager = vacancyManager;
        }


        [HttpGet]
        public ActionResult Index(string siteUrl, bool getFromDb = true) {
            return View(_vacancyManager.GetVacancies(new GetVacanciesRequest() {
                GetFromDb = getFromDb,
                SiteUrl = siteUrl
            }));
        }
    }
}